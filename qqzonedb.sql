/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : 127.0.0.1:3306
 Source Schema         : qqzonedb

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 17/02/2022 22:48:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_friend
-- ----------------------------
DROP TABLE IF EXISTS `t_friend`;
CREATE TABLE `t_friend` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` int DEFAULT NULL,
  `fid` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_friend_basic_uid` (`uid`),
  KEY `FK_friend_basic_fid` (`fid`),
  CONSTRAINT `FK_friend_basic_fid` FOREIGN KEY (`fid`) REFERENCES `t_user_basic` (`id`),
  CONSTRAINT `FK_friend_basic_uid` FOREIGN KEY (`uid`) REFERENCES `t_user_basic` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of t_friend
-- ----------------------------
BEGIN;
INSERT INTO `t_friend` VALUES (1, 1, 2);
INSERT INTO `t_friend` VALUES (2, 1, 3);
INSERT INTO `t_friend` VALUES (3, 1, 4);
INSERT INTO `t_friend` VALUES (4, 1, 5);
INSERT INTO `t_friend` VALUES (5, 2, 3);
INSERT INTO `t_friend` VALUES (6, 2, 1);
INSERT INTO `t_friend` VALUES (7, 2, 4);
INSERT INTO `t_friend` VALUES (8, 3, 1);
INSERT INTO `t_friend` VALUES (9, 3, 2);
INSERT INTO `t_friend` VALUES (10, 5, 1);
COMMIT;

-- ----------------------------
-- Table structure for t_host_reply
-- ----------------------------
DROP TABLE IF EXISTS `t_host_reply`;
CREATE TABLE `t_host_reply` (
  `id` int NOT NULL AUTO_INCREMENT,
  `content` varchar(500) NOT NULL,
  `hostReplyDate` datetime NOT NULL,
  `author` int NOT NULL,
  `reply` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_host_basic` (`author`),
  KEY `FK_host_reply` (`reply`),
  CONSTRAINT `FK_host_basic` FOREIGN KEY (`author`) REFERENCES `t_user_basic` (`id`),
  CONSTRAINT `FK_host_reply` FOREIGN KEY (`reply`) REFERENCES `t_reply` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of t_host_reply
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for t_reply
-- ----------------------------
DROP TABLE IF EXISTS `t_reply`;
CREATE TABLE `t_reply` (
  `id` int NOT NULL AUTO_INCREMENT,
  `content` varchar(500) NOT NULL,
  `replyDate` datetime NOT NULL,
  `author` int NOT NULL,
  `topic` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_reply_basic` (`author`),
  KEY `FK_reply_topic` (`topic`),
  CONSTRAINT `FK_reply_basic` FOREIGN KEY (`author`) REFERENCES `t_user_basic` (`id`),
  CONSTRAINT `FK_reply_topic` FOREIGN KEY (`topic`) REFERENCES `t_topic` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of t_reply
-- ----------------------------
BEGIN;
INSERT INTO `t_reply` VALUES (3, '给tifa顶顶贴!', '2021-07-14 16:16:54', 2, 8);
INSERT INTO `t_reply` VALUES (5, '哈哈，自己顶一下', '2021-07-14 16:30:49', 1, 8);
INSERT INTO `t_reply` VALUES (8, '回访一下，Cloud加油！', '2022-02-17 21:12:31', 1, 3);
COMMIT;

-- ----------------------------
-- Table structure for t_topic
-- ----------------------------
DROP TABLE IF EXISTS `t_topic`;
CREATE TABLE `t_topic` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `content` varchar(500) NOT NULL,
  `topicDate` datetime NOT NULL,
  `author` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_topic_basic` (`author`),
  CONSTRAINT `FK_topic_basic` FOREIGN KEY (`author`) REFERENCES `t_user_basic` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of t_topic
-- ----------------------------
BEGIN;
INSERT INTO `t_topic` VALUES (3, '我的空间开通了，先做自我介绍！', '大家好，我是云片！', '2021-06-18 11:25:30', 2);
INSERT INTO `t_topic` VALUES (8, '我的空间', '我的空间', '2021-07-14 16:16:40', 1);
INSERT INTO `t_topic` VALUES (9, '蒂法的日志', '今天克劳德要回来了', '2022-02-16 23:38:29', 1);
COMMIT;

-- ----------------------------
-- Table structure for t_user_basic
-- ----------------------------
DROP TABLE IF EXISTS `t_user_basic`;
CREATE TABLE `t_user_basic` (
  `id` int NOT NULL AUTO_INCREMENT,
  `loginId` varchar(20) NOT NULL,
  `nickName` varchar(50) NOT NULL,
  `pwd` varchar(20) NOT NULL,
  `headImg` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `loginId` (`loginId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of t_user_basic
-- ----------------------------
BEGIN;
INSERT INTO `t_user_basic` VALUES (1, 'u001', '蒂法', 'ok', 'tifa.jpeg');
INSERT INTO `t_user_basic` VALUES (2, 'u002', '克劳德', 'ok', 'cloud.png');
INSERT INTO `t_user_basic` VALUES (3, 'u003', '巴雷特', 'ok', 'barret.png');
INSERT INTO `t_user_basic` VALUES (4, 'u004', '萨菲罗斯', 'ok', 'SAFEILUOSI.jpeg');
INSERT INTO `t_user_basic` VALUES (5, 'u005', '爱丽丝', 'ok', 'Aerith.png');
COMMIT;

-- ----------------------------
-- Table structure for t_user_detail
-- ----------------------------
DROP TABLE IF EXISTS `t_user_detail`;
CREATE TABLE `t_user_detail` (
  `id` int NOT NULL,
  `realName` varchar(20) DEFAULT NULL,
  `tel` varchar(11) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `birth` datetime DEFAULT NULL,
  `star` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_detail_basic` FOREIGN KEY (`id`) REFERENCES `t_user_basic` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of t_user_detail
-- ----------------------------
BEGIN;
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
