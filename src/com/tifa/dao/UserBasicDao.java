package com.tifa.dao;

import com.tifa.pojo.UserBasic;

import java.util.List;

public interface UserBasicDao {
    //根据账号和密码获取特定用户信息
    UserBasic getUserBasic(String loginId, String pwd);

    //获取指定用户的所有好友列表
    List<UserBasic> getUserFriendList(UserBasic userBasic);

    //根据ID查询
    UserBasic getUserBasicByID(Integer id);

}
