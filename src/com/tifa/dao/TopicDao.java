package com.tifa.dao;

import com.tifa.pojo.Topic;
import com.tifa.pojo.UserBasic;

import java.util.List;

public interface TopicDao {
    //获取指定用户的所有日志。
    List<Topic> getTopicList(UserBasic userBasic);

    //添加日志
    void addTopic(Topic topic);

    //删除日志
    void deleteTopic(Topic topic);

    Topic getTopic(Integer id);
}
