package com.tifa.dao;

import com.tifa.pojo.Reply;
import com.tifa.pojo.Topic;

import java.util.List;

public interface ReplyDao {
    List<Reply> getReplyList(Topic topic);

    //添加回复
    void addReply(Reply reply);

    //删除回复
    void delReply(Integer id);
}
