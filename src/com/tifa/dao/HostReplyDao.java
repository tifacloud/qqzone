package com.tifa.dao;

import com.tifa.pojo.HostReply;

public interface HostReplyDao {
    HostReply getHostReplyByReplyId(Integer replyId);

    void deleteHostReplyById(Integer id);
}
