package com.tifa.dao.impl;

import com.tifa.dao.UserBasicDao;
import com.tifa.myssm.baseDao.BaseDao;
import com.tifa.pojo.UserBasic;

import java.util.List;

public class UserBasicDaoImpl extends BaseDao<UserBasic> implements UserBasicDao {
    @Override
    public UserBasic getUserBasic(String loginId, String pwd) {
        return super.load("select * from t_user_basic where loginId = ? and pwd = ?", loginId, pwd);
    }

    @Override
    public List<UserBasic> getUserFriendList(UserBasic userBasic) {
        var sql = "select fid as 'id' from t_friend where uid=?";
        return super.executeQuery(sql, userBasic.getId());
    }

    @Override
    public UserBasic getUserBasicByID(Integer id) {
        return super.load("select * from t_user_basic where id = ?", id);
    }
}
