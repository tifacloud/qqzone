package com.tifa.dao.impl;

import com.tifa.dao.TopicDao;
import com.tifa.myssm.baseDao.BaseDao;
import com.tifa.pojo.Topic;
import com.tifa.pojo.UserBasic;

import java.util.List;

/**
 * @Classname TopicDaoImpl
 * @Description
 * @Date 2022/2/13 21:12
 * @Created by hengyuan
 */
public class TopicDaoImpl extends BaseDao<Topic> implements TopicDao {
    @Override
    public List<Topic> getTopicList(UserBasic userBasic) {
        return super.executeQuery("select * from t_topic where author =?", userBasic.getId());
    }

    @Override
    public void addTopic(Topic topic) {
        executeUpdate("insert into t_topic values(0,?,?,?,?)", topic.getTitle(), topic.getContent(), topic.getTopicDate(), topic.getAuthor().getId());
    }

    @Override
    public void deleteTopic(Topic topic) {
        executeUpdate("delete from t_topic where id=?", topic.getId());
    }

    @Override
    public Topic getTopic(Integer id) {
        return super.load("select * from t_topic where id = ?", id);
    }

}
