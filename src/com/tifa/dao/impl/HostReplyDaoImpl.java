package com.tifa.dao.impl;

import com.tifa.dao.HostReplyDao;
import com.tifa.myssm.baseDao.BaseDao;
import com.tifa.pojo.HostReply;

public class HostReplyDaoImpl extends BaseDao<HostReply> implements HostReplyDao {
    @Override
    public HostReply getHostReplyByReplyId(Integer replyId) {
        return super.load("select * from t_host_reply where reply = ?", replyId);
    }

    @Override
    public void deleteHostReplyById(Integer id) {
        executeUpdate("delete from t_host_reply where id =? ", id);
    }
}
