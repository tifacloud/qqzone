package com.tifa.service.impl;

import com.tifa.dao.UserBasicDao;
import com.tifa.pojo.UserBasic;
import com.tifa.service.UserBasicService;

import java.util.ArrayList;
import java.util.List;

/**
 * @Classname UserBasicServiceImpl
 * @Description
 * @Date 2022/2/13 21:17
 * @Created by hengyuan
 */
public class UserBasicServiceImpl implements UserBasicService {
    private UserBasicDao userBasicDao = null;

    @Override
    public UserBasic login(String loginId, String pwd) {
        UserBasic userBasic = userBasicDao.getUserBasic(loginId, pwd);
        return userBasic;
    }

    @Override
    public List<UserBasic> getFriendList(UserBasic userBasic) {
        List<UserBasic> userbasicList = userBasicDao.getUserFriendList(userBasic);
        List<UserBasic> friendList = new ArrayList<>(userbasicList.size());
        for (var userItem : userbasicList) {
            userItem = userBasicDao.getUserBasicByID(userItem.getId());
            friendList.add(userItem);
        }
        return friendList;
    }

    @Override
    public UserBasic getUserBasicById(Integer id) {
        return userBasicDao.getUserBasicByID(id);
    }
}
