package com.tifa.service.impl;

import com.tifa.dao.TopicDao;
import com.tifa.pojo.Reply;
import com.tifa.pojo.Topic;
import com.tifa.pojo.UserBasic;
import com.tifa.service.ReplyService;
import com.tifa.service.TopicService;
import com.tifa.service.UserBasicService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * @Classname TopicServiceImpl
 * @Description
 * @Date 2022/2/13 21:28
 * @Created by hengyuan
 */
public class TopicServiceImpl implements TopicService {
    private TopicDao topicDao = null;
    private ReplyService replyService = null;
    private UserBasicService userBasicService = null;

    @Override
    public List<Topic> getTopicList(UserBasic userBasic) {
        return topicDao.getTopicList(userBasic);
    }

    @Override
    public Topic getTopic(Integer id) {
        var topic = topicDao.getTopic(id);
        UserBasic author = topic.getAuthor();
        author = userBasicService.getUserBasicById(author.getId());
        topic.setAuthor(author);
        return topic;
    }

    @Override
    public Topic getTopicById(Integer id) {
        var topic = getTopic(id);
        List<Reply> replyList = replyService.getReplyListByTopicId(topic.getId());
        topic.setReplyList(replyList);
        return topic;
    }

    @Override
    public void deleteTopic(Integer topicId) {
        Topic topic = getTopicById(topicId);
        for (var reply : topic.getReplyList()) {
            replyService.deleteReply(reply.getId());
        }
        topicDao.deleteTopic(new Topic(topicId));
    }
}
