package com.tifa.service.impl;

import com.tifa.dao.HostReplyDao;
import com.tifa.pojo.HostReply;
import com.tifa.service.HostReplyService;

public class HostReplyServiceImpl implements HostReplyService {
    private HostReplyDao hostReplyDao;

    @Override
    public HostReply getHostReplyByReplyId(Integer replyId) {
        return hostReplyDao.getHostReplyByReplyId(replyId);
    }

    @Override
    public void deleteHostReplyById(Integer id) {
        hostReplyDao.deleteHostReplyById(id);
    }
}
