package com.tifa.service.impl;

import com.tifa.dao.ReplyDao;
import com.tifa.pojo.HostReply;
import com.tifa.pojo.Reply;
import com.tifa.pojo.Topic;
import com.tifa.pojo.UserBasic;
import com.tifa.service.HostReplyService;
import com.tifa.service.ReplyService;
import com.tifa.service.UserBasicService;

import java.util.List;
import java.util.Objects;

public class ReplyServiceImpl implements ReplyService {

    private ReplyDao replyDao;
    //这里引入的是POJO的service接口而不是对应POJO的DAO。因为我们需要的是逻辑而不是内部的细节。
    private HostReplyService hostReplyService;
    private UserBasicService userBasicService;

    @Override
    public List<Reply> getReplyListByTopicId(Integer topicId) {
        var replyList = replyDao.getReplyList(new Topic(topicId));
        for (int i = 0; i < replyList.size(); i++) {
            Reply reply = replyList.get(i);
            //将关联的作者信息设置进去
            UserBasic userBasic = userBasicService.getUserBasicById(reply.getAuthor().getId());
            reply.setAuthor(userBasic);
            HostReply hostReplyByReply = hostReplyService.getHostReplyByReplyId(reply.getId());
            reply.setHostReply(hostReplyByReply);
        }
        return replyList;
    }

    @Override
    public void addReply(Reply reply) {
        replyDao.addReply(reply);
    }

    @Override
    public void deleteReply(Integer id) {
        HostReply hostReply = hostReplyService.getHostReplyByReplyId(id);
        if (Objects.nonNull(hostReply)) {
            hostReplyService.deleteHostReplyById(hostReply.getId());
        }
        replyDao.delReply(id);

    }
}
