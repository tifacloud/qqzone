package com.tifa.service;

import com.tifa.pojo.UserBasic;

import java.util.List;

/**
 * @Classname UserBasicService
 * @Description
 * @Date 2022/2/13 21:16
 * @Created by hengyuan
 */
public interface UserBasicService {
    UserBasic login(String loginId, String pwd);

    List<UserBasic> getFriendList(UserBasic userBasic);

    UserBasic getUserBasicById(Integer id);

}
