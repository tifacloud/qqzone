package com.tifa.service;

import com.tifa.pojo.HostReply;

public interface HostReplyService {

    HostReply getHostReplyByReplyId(Integer replyId);

    void deleteHostReplyById(Integer id);

}
