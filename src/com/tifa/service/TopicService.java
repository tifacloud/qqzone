package com.tifa.service;/**
 * @Classname TopicService
 * @Description
 * @Date 2022/2/13 21:27
 * @Created by Ahyer
 */

import com.tifa.pojo.Topic;
import com.tifa.pojo.UserBasic;

import java.util.List;

/**
 * @Classname TopicService
 * @Description
 * @Date 2022/2/13 21:27
 * @Created by hengyuan
 */
public interface TopicService {
    //查询特定用户的日志列表
    List<Topic> getTopicList(UserBasic userBasic);

    Topic getTopicById(Integer id);

    Topic getTopic(Integer id);

    void deleteTopic(Integer topicId);

}
