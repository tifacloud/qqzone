package com.tifa.service;

import com.tifa.pojo.Reply;

import java.util.List;

public interface ReplyService {

    List<Reply> getReplyListByTopicId(Integer topicId);

    void addReply(Reply reply);

    void deleteReply(Integer id);
}
