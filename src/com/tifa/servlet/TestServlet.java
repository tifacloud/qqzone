package com.tifa.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @Classname TestServlet
 * @Description
 * @Date 2022/2/15 18:08
 * @Created by hengyuan
 */
@WebServlet("*.xx")
public class TestServlet extends ViewBaseServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String requestAttrName = "helloRequestAttr";
        String requestAttrValue = "helloRequestAttr-VALUE";

        session.setAttribute("hello", "你好");
        super.processTemplate("test", request, resp);
    }
}
