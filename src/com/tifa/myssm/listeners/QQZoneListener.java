package com.tifa.myssm.listeners;

import com.tifa.myssm.ioc.BeanFactory;
import com.tifa.myssm.ioc.impl.ClassPathXmlApplicationContext;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;


@WebListener
public class QQZoneListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        var application = sce.getServletContext();
        sce.getServletContext().setRequestCharacterEncoding("UTF-8");
        BeanFactory beanFactory = new ClassPathXmlApplicationContext();
        application.setAttribute("beanFactory", beanFactory);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ServletContextListener.super.contextDestroyed(sce);
    }
}
