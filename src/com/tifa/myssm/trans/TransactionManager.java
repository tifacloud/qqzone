package com.tifa.myssm.trans;

import com.tifa.myssm.baseDao.ConnUtil;

import java.sql.SQLException;

/**
 * @Classname TransactionManager
 * @Description
 * @Date 2022/2/13 20:37
 * @Created by hengyuan
 */
public class TransactionManager {
    public static void beginTrans() throws Exception {
        var connection = ConnUtil.getConn();
        connection.setAutoCommit(false);
    }

    public static void commit() throws Exception {
        var connection = ConnUtil.getConn();
        connection.commit();
        ConnUtil.close();
    }

    public static void rollback() throws SQLException {
        var connection = ConnUtil.getConn();
        connection.rollback();
        ConnUtil.close();
    }
}
