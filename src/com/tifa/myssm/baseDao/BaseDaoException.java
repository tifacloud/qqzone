package com.tifa.myssm.baseDao;

public class BaseDaoException extends RuntimeException {
    public BaseDaoException(String msg) {
        super(msg);
    }
}
