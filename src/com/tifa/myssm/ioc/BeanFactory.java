package com.tifa.myssm.ioc;

public interface BeanFactory {
    Object getBean(String id);
}
