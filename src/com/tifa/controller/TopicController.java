package com.tifa.controller;

import com.tifa.pojo.Topic;
import com.tifa.pojo.UserBasic;
import com.tifa.service.ReplyService;
import com.tifa.service.TopicService;

import javax.servlet.http.HttpSession;
import java.util.List;

public class TopicController {
    private TopicService topicService;
    private ReplyService replyService;

    public String topicDetail(Integer id, HttpSession session) {
        Topic topic = topicService.getTopicById(id);
        session.setAttribute("topic", topic);
        return "frames/detail";
    }

    /**
     * 删除日志
     *
     * @param topicId
     */
    public String delTopic(Integer topicId) {
        topicService.deleteTopic(topicId);
        //先删除所有的回复
        return "redirect:topic.do?operate=getTopicList";
        //再删除日志。
    }

    public String getTopicList(HttpSession session) {
        var userBasic = (UserBasic) session.getAttribute("userBasic");
        List<Topic> topicList = topicService.getTopicList(userBasic);
        userBasic.setTopicList(topicList);
        session.setAttribute("friend", userBasic);
        return "frames/main";
    }
}
