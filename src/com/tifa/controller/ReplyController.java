package com.tifa.controller;

import com.tifa.pojo.Reply;
import com.tifa.pojo.Topic;
import com.tifa.pojo.UserBasic;
import com.tifa.service.ReplyService;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;

public class ReplyController {

    private ReplyService replyService;

    /**
     * 添加回复
     *
     * @param content
     * @param session
     * @param topicId
     * @return
     */
    public String addReply(String content, HttpSession session, Integer topicId) {
        var author = (UserBasic) session.getAttribute("userBasic");
        var reply = new Reply(content, LocalDateTime.now(), author, new Topic(topicId));
        replyService.addReply(reply);
        //detail页面，应该重新刷新一下当前页面。
        return "redirect:topic.do?operate=topicDetail&id=" + topicId;

    }

    /**
     * 删除回复
     *
     * @param replyId
     * @return
     */
    public String delReply(Integer replyId, HttpSession session) {
        replyService.deleteReply(replyId);
        var topic = (Topic) session.getAttribute("topic");
        return "redirect:topic.do?operate=topicDetail&id=" + topic.getId();
    }

}
