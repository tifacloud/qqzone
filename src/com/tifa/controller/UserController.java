package com.tifa.controller;

import com.tifa.pojo.Topic;
import com.tifa.pojo.UserBasic;
import com.tifa.service.TopicService;
import com.tifa.service.UserBasicService;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Objects;

/**
 * @Classname UserController
 * @Description
 * @Date 2022/2/13 21:32
 * @Created by hengyuan
 */
public class UserController {
    private UserBasicService userBasicService;
    private TopicService topicService;
    /**
     * 帮忙做登录验证
     * @param loginId 登录名
     * @param pwd 密码
     * @param session
     * @return
     */
    public String login(String loginId, String pwd, HttpSession session) {
        UserBasic userBasic = userBasicService.login(loginId, pwd);
        if (Objects.nonNull(userBasic)) {
            //查询好友列表
            List<UserBasic> friendList = userBasicService.getFriendList(userBasic);
            //得到日志列表
            List<Topic> topicList = topicService.getTopicList(userBasic);
            userBasic.setTopicList(topicList);
            userBasic.setFriendList(friendList);
            //登陆成功，将用户保存进session中，这是登陆者的key
            session.setAttribute("userBasic", userBasic);
            //这个表示进入的是谁的空间。
            session.setAttribute("friend", userBasic);
            return "index";
        }
        return "login";
    }

    public String index() {
        return "index";
    }

    public String friend(Integer id, HttpSession session) {
        var currentFriend  = userBasicService.getUserBasicById(id);
        List<Topic> topicList = topicService.getTopicList(currentFriend);
        currentFriend.setTopicList(topicList);
        session.setAttribute("friend", currentFriend);
        return index();
    }
}
